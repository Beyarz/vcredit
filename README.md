# VCredit

Offline card number validation supported by Luhn algorithm

## Supported types

- VISA
- MasterCard
- American Express

## Contribution

### Dev

`yarn run dev`

### Build

`yarn run build`

### Clean (remove dist)

`yarn run clean`
